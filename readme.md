![laravel](resources/assets/img/logo-laravel.svg)
![laravel-doctrine](resources/assets/img/logo-laravel-doctrine.svg)

Larvel skeleton app with doctrine

## Quickstart

- Fork this repo so you can add your changes
- Clone your fork
- Install dependencies by running `composer install`
- Copy the `.env.example` file and rename it to `.env`
- Edit the `.env` file and enter your info
- Run `php artisan key:generate` to generate an application key, this is used for hashing and stuff
- Update your db schema by running: `php artisan doctrine:schema:update --force` 
- Validate your db schema by running: `php artisan doctrine:schema:validate`
- Browse your `APP_URL` and you should see your new Laravel application!

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## About Laravel Doctrine

##### A drop-in Doctrine ORM 2 implementation for Laravel 5+

Doctrine 2 is an object-relational mapper (ORM) for PHP that provides transparent persistence for PHP objects. It uses the Data Mapper pattern at the heart, aiming for a complete separation of your domain/business logic from the persistence in a relational database management system.

The benefit of Doctrine for the programmer is the ability to focus on the object-oriented business logic and worry about persistence only as a secondary problem. This doesn’t mean persistence is downplayed by Doctrine 2, however it is our belief that there are considerable benefits for object-oriented programming if persistence and entities are kept separated.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## debug artisan (CLI)
Add the following lines to .bashrc
`alias artisan='php artisan'
function artisandebug() { sudo ~/php_cli_debug_toggle.sh; artisan $@; sudo ~/php_cli_debug_toggle.sh; }``
artisandebug [CMD]