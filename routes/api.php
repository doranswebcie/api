<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    /** @var \App\Entities\User $identity */
    $identity = $request->user();
    return response()->json([
        "identity" => $identity->getArrayCopy(),
    ]);
});

Route::get('/test', function (Request $request) {
    /** @var \App\Entities\User $identity */
    $identity = $request->user();
    return response()->json([
        "identity" => $identity === null ? $identity : $identity->getArrayCopy(),
    ]);
});

Route::group(
    [
        'prefix' => 'auth',
        'middleware' => [
            'api',
        ]
    ], function () {
    Route::post('login', \App\Api\Controllers\AuthController::class . '@login');
    Route::post('logout', \App\Api\Controllers\AuthController::class . '@logout');
    Route::post('refresh', \App\Api\Controllers\AuthController::class . '@refresh');
    Route::get('me', \App\Api\Controllers\AuthController::class . '@me');
});

// region Dorans-Competition

Route::group(
    [
        'prefix' => 'v1',
        'middleware' => [
            'auth:api',
//            'auth.admin', // TODO: check on user role if allowed crud or just read
            \Barryvdh\Cors\HandleCors::class,
        ]
    ], function () {

    Route::resource('user', \App\Api\Controllers\Auth\UserController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

    Route::resource('competition', \App\Api\Controllers\Competition\CompetitionController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

    Route::resource('game', \App\Api\Controllers\Competition\GameController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

    Route::resource('team', \App\Api\Controllers\Competition\TeamController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

    Route::resource('player', \App\Api\Controllers\Competition\PlayerController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

    Route::resource('summoner', \App\Api\Controllers\Competition\SummonerController::class, [
        'except' => [
            'create',
            'edit',
        ]
    ]);

});

// endregion

// region Dorans-Wordpress

Route::group(
    [
        'prefix' => 'public/v1',
        'middleware' => [
            'auth:token',
        ]
    ], function () {


    Route::resource('competition', \App\Api\Controllers\Competition\CompetitionController::class, [
        'only' => [
            'index',
            'show',
        ]
    ]);

    Route::resource('game', \App\Api\Controllers\Competition\GameController::class, [
        'only' => [
            'index',
            'show',
        ]
    ]);

    Route::resource('team', \App\Api\Controllers\Competition\TeamController::class, [
        'only' => [
            'index',
            'show',
        ]
    ]);

//    Route::resource('player', \App\Api\Controllers\Competition\PlayerController::class, [
//        'only' => [
//            'index',
//            'show',
//        ]
//    ]);

});

// endregion
