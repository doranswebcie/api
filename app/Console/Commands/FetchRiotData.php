<?php

namespace App\Console\Commands;

use App\Entities\Lol\Summoner;
use App\Riot\ApiService;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class FetchRiotData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'riot:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches summoner data from Riot API';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->comment(Inspiring::quote());

        $em = app('em');
        $summoners = $em->getRepository(Summoner::class)->findAll();

        $api = new ApiService();
        $api->getID($summoners);

        $api->getRankings($summoners);
        $this->comment('done');
    }
}
