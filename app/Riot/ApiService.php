<?php

namespace App\Riot;

use App\Entities\Lol\Queue;
use App\Entities\Lol\Summoner;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Illuminate\Support\Facades\Log;
use LeagueWrap\Results\Result;

class ApiService
{
    const IS_DEBUG_LOGGING = true;

    /**
     * List with queue types that are monitored for changes
     *
     * @var array
     */
    public $monitoredQueueTypes = [
        "RANKED_SOLO_5x5",
        "RANKED_FLEX_SR",
    ];

    # region move to API
    /**
     * TODO: move to static class, this is also used in \Dorans\Competition\ShortCodes\TeamDetail
     * All league tiers from lowest to highest value
     * @var array
     */
    protected $tiers = [
        "provisional",
        "bronze",
        "silver",
        "gold",
        "platinum",
        "diamond",
        "master",
        "challenger",
    ];
    protected $divisions = [
        "v",
        "iv",
        "iii",
        "ii",
        "i",
    ];
    # endregion

    /** @var EntityManagerInterface */
    protected $em;

    protected $apiClient;

    public function __construct()
    {
        $this->em = app('em'); // TODO: inject
    }

    /**
     * @param Summoner[] $summoners
     * @throws \Exception
     */
    public function getID($summoners)
    {
        $apiClient = $this->getApiClient();
        $summoners = $this->filterPlayersBySummonerApiId($summoners);
        $fetchApiIds = [];
        foreach ($summoners as $summoner) {
            if (empty($summoner->getTag())) {
                Log::info("Summoner \"" . $summoner->getId() . " :: " . $summoner->getTag() . "\" has an empty tag.");
                continue;
            }
            $fetchApiIds[] = $summoner->getTag();
        }
        if (empty($fetchApiIds)) {
            return;
        }
        $this->updatePlayerApiId($summoners, $apiClient->getSummonerInfo($fetchApiIds));
    }

    /**
     * @param Summoner[] $summoners
     * @throws \Exception
     */
    public function getRankings($summoners)
    {
        $apiIds = [];
        $summonersOrderedByApi = [];
        foreach ($summoners as $summoner) {
            if ($summoner->getApi() === null) {
                continue;
            }

            $apiIds[] = $summoner->getApi();
            $summonersOrderedByApi[$summoner->getApi()] = $summoner;
        }

        $rankings = $this->getApiClient()->getRankings($apiIds);
        $this->updateRankings($summonersOrderedByApi, $rankings);
    }

    /**
     * @param Summoner[] $summoners
     * @return Summoner[]
     */
    protected function filterPlayersBySummonerApiId($summoners)
    {
        $noApiIdList = [];
        foreach ($summoners as $summoner) {
            if (!empty($summoner->getApi())) {
                continue;
            }
            $noApiIdList[$summoner->getTag()] = $summoner;
        }
        return $noApiIdList;
    }

    /**
     * TODO: rewrite to handle one summoner at a time
     * TODO: probably smarter to make the API call and the DB update part of the same function/loop
     * TODO: should probably flush more often, to reduce the chance of a DB timeout
     *
     * @param Summoner[] $summoners
     * @param Result[] $apiData
     * @throws \Exception
     */
    protected function updatePlayerApiId($summoners, $apiData)
    {
        foreach ($apiData as $entry) {
            $entry = $entry->getContent();
            if (!isset($entry["name"])) {
                continue;
            }

            if (!isset($summoners[$entry["name"]])) {
                Log::info("Player \"" . $entry["name"] . "\" was returned, but not requested.");
                continue;
            }

            $summoner = $summoners[$entry["name"]];
            $summoner->setApi($entry["id"]);

            try {
                $this->em->persist($summoner);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                throw $e;
            }
        }
        $this->em->flush();
    }

    /**
     * TODO: probably smarter to make the API call and the DB update part of the same function/loop
     * TODO: should probably flush more often, to reduce the chance of a DB timeout
     *
     * @param Summoner[] $summoners
     * @param Result[] $apiData
     * @throws \Exception
     */
    protected function updateRankings($summoners, $apiData)
    {
        $hydrator = new DoctrineObject($this->em);

        foreach ($apiData as $entry) {
            $entry = $entry->getContent();
            if (!is_array($entry)) {
                Log::critical("Expected array in BatchResult.");
                throw new \InvalidArgumentException();
            }

            if (isset($entry["status"])) {
                Log::critical($entry["status"]["status_code"] === 429 ? "Rate limit exceeded" : "Response error");
                continue;
            }

            if (empty($entry)) {
                // TODO: should player be set to unranked?
                continue;
            }

            try {
                $summoner = $this->getPlayerByRanking($summoners, $entry);
                if ($summoner === null) {
                    continue;
                }
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
                continue;
            }

//            $highestTier = null;
//            $highestDivision = null;
            foreach ($entry as $ranking) {
                if (!in_array($ranking["queueType"], $this->monitoredQueueTypes)) {
                    continue;
                }

                $this->persistRanking($summoner, $ranking, $hydrator);

                # region move to API
//                $tier = $position["tier"];
//                $leagueEntry = $position->get("playerOrTeam");
//                if ($leagueEntry === null) {
//                    Log::error("Summoner \"" . $summoner->getId() . " :: " . $summoner->getTag() . "\" has an unexpected LeagueEntry.");
//                    continue;
//                }
//
//                $division = $leagueEntry->get("rank");
//
//                $tierValue = array_keys($this->tiers, strtolower($tier));
//                $highestTierValue = array_keys($this->tiers, strtolower($highestTier));
//                $divisionValue = array_keys($this->divisions, strtolower($division));
//                $highestDivisionValue = array_keys($this->divisions, strtolower($highestDivision));
//                if (is_null($highestTier) //first iteration
//                    || end($tierValue) > end($highestTierValue) //higher tier so skip division check
//                    || (end($tierValue) == end($highestTierValue) && end($divisionValue) > end($highestDivisionValue)) //same tier but higher division
//                ) {
//                    $highestTier = $tier;
//                    $highestDivision = $division;
//                }
                # endregion
            }

            # region move to API
//            if ($highestDivision != $player->getRankDivision() || $highestTier != $player->getRankTier()) {
//                $player->setRankTier($highestTier);
//                $player->setRankDivision($highestDivision);
//                $changed = true;
//            }
//
//            if ($changed) {
//                $result = $this->em->persist($player);
//
//                if (!$result) {
//                    Log::critical("Summoner \"" . $summoner->getId() . " :: " . $summoner->getTag() . "\" could not be persisted.");
//                }
//            }
            # endregion

            $this->em->flush();
        }
    }

    /**
     * @return ApiClient
     */
    protected function getApiClient()
    {
        if ($this->apiClient === null) {
            $this->apiClient = new ApiClient(self::IS_DEBUG_LOGGING);
        }
        return $this->apiClient;
    }

    /**
     * @param Summoner[] $summoners
     * @param array $summonerRankings
     * @return Summoner
     * @throws \Exception
     */
    protected function getPlayerByRanking(array $summoners, array $summonerRankings)
    {
        $ranking = $summonerRankings[0];
        if (!array_key_exists("summonerId", $ranking)) {
            throw new \Exception("Unexpected return data.");
        }

        if (array_key_exists($ranking["summonerId"], $summoners)) {
            return $summoners[$ranking["summonerId"]];
        }

        Log::info("Summoner \"" . $ranking["playerOrTeamId"] . " :: " . $ranking["playerOrTeamName"] . "\" was returned, but not requested.");
        return null;
    }

    /**
     * @param Summoner $summoner
     * @param array $ranking
     * @param DoctrineObject $hydrator
     * @throws \Exception
     */
    protected function persistRanking(Summoner $summoner, array $ranking, DoctrineObject $hydrator)
    {
        try {
            $queue = $this->getQueue($summoner, $ranking);

            // Update entity with the new data
            $queue = $hydrator->hydrate(
                [
                    "summoner" => $summoner,
                    "tier" => $ranking["tier"],
                    "division" => $ranking["rank"],
                    "type" => $ranking["queueType"],
                ],
                $queue
            );
            $this->em->persist($queue);
        } catch (DBALException $e) {
            Log::error($e->getMessage());
            // This is needed when lazy loading the summoner's existing Queue entities
            if (!$this->em->getConnection()->ping()) {
                $this->em->getConnection()->close();
                $this->em->getConnection()->connect();
                $this->persistRanking($summoner, $ranking, $hydrator);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param Summoner $summoner
     * @param array $ranking
     * @return Queue
     */
    protected function getQueue($summoner, $ranking)
    {
        foreach ($summoner->getQueues() as $queue) {
            if ($queue->getType() !== $ranking["queueType"]) {
                continue;
            }
            return $queue;
        }

        $queue = new Queue();
        $summoner->addQueues([$queue]);
        return $queue;
    }
}