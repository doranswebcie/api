<?php

namespace App\Riot;

use Cache\Adapter\Illuminate\IlluminateCachePool;
use Illuminate\Cache\Repository;
use Illuminate\Support\Facades\Log;
use LeagueWrap\Api\League;
use LeagueWrap\Client;
use LeagueWrap\Exceptions\RateLimitReachedException;
use LeagueWrap\Results\Result;

// TODO: fix LeagueWrap\Api::collection
class ApiClient
{
    const KEY = "RGAPI-79a8af44-e62b-453a-be9a-fd91e70483a6";// "RGAPI-2752776e-a350-4dbe-ba64-9d3ffafbb202"; // TODO: enable prod key
    const REGION = "euw";

    const LIMITS = [
        1 => 20,
        120 => 100,
    ];

    /** @var bool */
    protected $isLogging;

    /** @var Client */
    protected $apiClient;

    protected $summonerClient;
    protected $leagueClient;

    protected $startedAt;

    /**
     * ApiClient constructor.
     * @param bool $isLogging
     */
    public function __construct($isLogging = false)
    {
        $this->isLogging = $isLogging;

        $apiClient = new Client(static::KEY, static::REGION);

        # region TODO: inject
        $store = app(Repository::class)->getStore();
        $pool = new IlluminateCachePool($store);
        # region TODO: inject

        $apiClient->setAppLimits(static::LIMITS, $pool);

        $this->apiClient = $apiClient;
    }

    /**
     * @param $data
     * @throws \Exception
     */
    protected function writeLog($data)
    {
        Log::warning($data);
    }

    /**
     * @param string[] $summoners
     * @return \LeagueWrap\Results\Result[]
     * @throws \Exception
     */
    public function getSummonerInfo(array $summoners)
    {
        $responses = [];

        try {
            foreach ($summoners as $summoner) {

                $success = false;
                $tries = 0;
                while ($success === false && $tries < 2) {
                    $tries++;
                    try {
                        /** @var Result $result */
                        $result = $this->getSummonerClient()->byName($summoner);
                    } catch (RateLimitReachedException $rex) {
                        $this->backOff();
                        continue;
                    }
                    $success = true;
                }

                $responses[] = $result;
            }
            $this->writeLog("getSummonerInfo: " . count($responses));
            return $responses;
        } catch (\Exception $e) {
            $this->writeLog("Server error");
            throw new \Exception("Server error", 0, $e);
        }
    }

    /**
     * @param int[] $summoners
     * @return \LeagueWrap\Results\Result[]
     * @throws \Exception
     */
    public function getRankings(array $summoners)
    {
        $responses = [];

        try {
//            $batches = $this->createBatches($summoners);

            $this->startedAt = time();
            foreach ($summoners as $summoner) {

                $success = false;

                $tries = 0;
                while ($success === false && $tries < 2) {
                    $tries++;
                    try {
                        /** @var Result $result */
                        $result = $this->getLeagueClient()->positionById($summoner);
                    } catch (RateLimitReachedException $rex) {
                        $this->backOff();
                        continue;
                    }
                    $success = true;
                }
                $responses[] = $result;
            }
            $this->writeLog("getRankings: " . count($responses));
            return $responses;
        } catch (\Exception $e) {
            $this->writeLog("Server error");
            throw new \Exception("Server error", 0, $e);
        }
    }

    /**
     * @return \LeagueWrap\Api\Summoner
     */
    protected function getSummonerClient()
    {
        if ($this->summonerClient === null) {
            $this->summonerClient = $this->apiClient->summoner(); // Load up the summoner request object.
        }
        return $this->summonerClient;
    }

    /**
     * @return League
     */
    protected function getLeagueClient()
    {
        if ($this->leagueClient === null) {
            $this->leagueClient = $this->apiClient->league(); // Load up the league request object.
        }
        return $this->leagueClient;
    }

    /**
     * 1 is the smallest API batch time limit
     *
     * @param array $entries
     * @return array
     */
    protected function createBatches(array $entries)
    {
        return array_chunk($entries, static::LIMITS[1]);
    }

    /**
     * 120 is the upper API batch time limit
     */
    protected function backOff()
    {
        $timeElapsed = time() - $this->startedAt;
        $backOffTime = 120 - $timeElapsed;
        sleep($backOffTime > 0 ? $backOffTime : 1);
    }
}