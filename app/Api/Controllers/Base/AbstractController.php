<?php

namespace App\Api\Controllers\Base;

use App\Entities\ArrayCopyable;
use App\Entities\Base\AbstractEntity;
use App\Http\Controllers\Controller;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\PersistentCollection;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

abstract class AbstractController extends Controller
{
    const ENTITY_CLASS = null;

    const NOT_FOUND_ERROR = "not found";

    public function __construct()
    {
        if (static::ENTITY_CLASS === null) {
            throw new \Exception("constant \"ENTITY_CLASS\" cannot be NULL");
        }

        parent::__construct();
    }

    protected abstract function validateRequest(Request $request);

    /**
     * TODO: https://laravel.com/docs/5.4/validation#form-request-validation
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bodyContent = $request->all();
//        $data = $request->json();
        try {
            $this->validateRequest($request);

            $entityClass = static::ENTITY_CLASS;
            $hydrator = new DoctrineObject($this->em);
            $entity = $hydrator->hydrate($bodyContent, new $entityClass());

            $this->em->persist($entity);
            $this->em->flush();
        } catch (ValidationException $ve) {
            return response()->json(
                [
                    "error" => $ve->getMessage(),
                    "feedback" => $ve->validator->errors(),
                ],
                400);
        } catch (\Exception $e) {
            return response()->json(
                [
                    "error" => $e->getMessage(),
                ],
                500);
        }

        return response()->json(["entity" => $this->getIdentifier($entity), "success" => "created",], 201);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $criteria = $this->getCriteriaParamFromRequest($request);
        $orderBy = $this->getOrderByParamFromRequest($request);
        $include = $this->getIncludeParamFromRequest($request);

        try {
            $entities = $this->getEntities($criteria, $orderBy);
        } catch (\Exception $e) {
            return response()->json(
                [
                    "error" => $e->getMessage(),
                ],
                500);
        }
        foreach ($entities as &$entity) {
            // TODO: this might also be useful for $this::show
            if ($entity instanceof ArrayCopyable) {
                $entity = $this->prepareResponse($entity, $include);
                continue;
            }

            if (gettype($entity) !== 'array' || !isset($entity[0])) {
                throw new \Exception('Unexpected entry type "' . gettype($entity) . '"');
            }

            $entity[0] = $this->prepareResponse($entity[0], $include);
        }

        return response()->json(["entities" => $entities,]);
    }


    /**
     * @param $criteria
     * @param $orderBy
     * @return AbstractEntity[]
     */
    protected function getEntities($criteria, $orderBy)
    {
        /** @var AbstractEntity[] $entities */
        return $this->em->getRepository(static::ENTITY_CLASS)->findBy(
            $criteria,
            $orderBy
        );
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            $include = $this->getIncludeParamFromRequest($request);
            $entity = $this->getEntity($id, $include);
            if ($entity === null) {
                throw new \Exception(self::NOT_FOUND_ERROR);
            }
        } catch (\Exception $e) {
            return response()->json(
                [
                    "error" => $e->getMessage(),
                ],
                $e->getMessage() === self::NOT_FOUND_ERROR ? 404 : 500);
        }

        $entity = $this->prepareResponse($entity, $include);

        return response()->json(["entity" => $entity,]);
    }

    /**
     * TODO: https://laravel.com/docs/5.4/validation#form-request-validation
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bodyContent = $request->all(); // TODO: merge current entity data (related objects) with new bodyContent
//        $data = $request->json();
        try {
            $this->validateRequest($request);

            $entity = $this->getEntity($id);
            if ($entity === null) {
                throw new \Exception(self::NOT_FOUND_ERROR);
            }

            $hydrator = new DoctrineObject($this->em);
            $entity = $hydrator->hydrate($bodyContent, $entity);

            $this->em->persist($entity);
            $this->em->flush();
        } catch (ValidationException $ve) {
            return response()->json(
                [
                    "error" => $ve->getMessage(),
                    "feedback" => $ve->validator->errors(),
                ],
                400);
        } catch (\Exception $e) {
            return response()->json(
                [
                    "error" => $e->getMessage(),
                ],
                $e->getMessage() === self::NOT_FOUND_ERROR ? 404 : 500);
        }

        return response()->json(["entity" => $this->getIdentifier($entity), "success" => "updated",]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $entity = $this->getEntity($id);
            if ($entity === null) {
                throw new \Exception(self::NOT_FOUND_ERROR);
            }

            $this->em->remove($entity);
            $this->em->flush();
        } catch (\Exception $e) {
            return response()->json(
                [
                    "error" => $e->getMessage(),
                ],
                $e->getMessage() === self::NOT_FOUND_ERROR ? 404 : 500);
        }

        return response()->json(["success" => "deleted",]);
    }


    /**
     * TODO: this should probably make use of a Repository instead of calling the entity manager
     *
     * @param int $id
     * @param array $include
     * @return AbstractEntity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function getEntity($id, array $include = [])
    {
        /** @var AbstractEntity $entity */
        $entity = $this->em->find(static::ENTITY_CLASS, $id);
        return $entity;
    }

    /**
     * prepare data for serialization
     *
     * @param ArrayCopyable $entity
     * @param array $includes
     * @return mixed
     */
    protected function prepareResponse(ArrayCopyable $entity, array $includes)
    {
        $relations = $this->handleIncludes($entity, $includes);
        // Get object as array (only public properties are available to the serializer)
        $entity = $entity->getArrayCopy();
        // Add the related object data to the entity array
        $entity = $this->addToArray($entity, $relations);
        return $entity;
    }

    /**
     * Iterate through the includes and return the Entity's related data
     *
     * @param ArrayCopyable $entity
     * @param array $includes
     * @return array
     */
    protected function handleIncludes(ArrayCopyable $entity, array $includes)
    {
        $relations = [];
        foreach ($includes as $property) {
            $getter = 'get' . ucfirst($property);
            if (!method_exists($entity, $getter)) {
                continue;
            }
            /** @var ArrayCopyable|PersistentCollection $relatedEntity */
            $relatedEntity = $entity->$getter();
            if ($relatedEntity instanceof PersistentCollection) {
                $relatedEntity = $relatedEntity->toArray();
                /** @var ArrayCopyable $related */
                foreach ($relatedEntity as &$related) {
                    $related = $this->getRelatedAsArray($related, $includes, $property);
                }
            } else {
                $relatedEntity = $relatedEntity->getArrayCopy();
            }
            $relations[$property] = $relatedEntity;
        }
        return $relations;
    }

    /**
     * Can be overwritten to customize the data of a related entity.
     *
     * @param ArrayCopyable $related
     * @param array $includes
     * @param string $property
     * @return array
     */
    protected function getRelatedAsArray(ArrayCopyable $related, array $includes, $property)
    {
        return $related->getArrayCopy();
    }

    /**
     * @param array $result
     * @param array $addToResult
     * @return array
     */
    protected function addToArray(array $result, array $addToResult)
    {
        foreach ($addToResult as $relation => $data) {
            $result[$relation] = $data;
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getCriteriaParamFromRequest(Request $request)
    {
        $criteria = $request->query('criteria', []);
        if (is_string($criteria)) {
            $criteria = [];
        }
        return $criteria;
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getOrderByParamFromRequest(Request $request)
    {
        $orderBy = $request->query('orderBy', ['id' => Criteria::DESC]);
        return $orderBy;
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getIncludeParamFromRequest(Request $request)
    {
        $include = $request->query('include', []);
        if (is_string($include)) {
            $include = [];
        }
        return $include;
    }

    /**
     * @param $entity
     * @return int
     */
    protected function getIdentifier($entity)
    {
        /** @var AbstractEntity $entity */
        return $entity->getId();
    }
}
