<?php

namespace App\Api\Controllers;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Tymon\JWTAuth\JWTGuard;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', [
            'except' => [
                'login',
                'refresh', // TODO: https://github.com/tymondesigns/jwt-auth/issues/1387 AND https://github.com/tymondesigns/jwt-auth/issues/1442
            ]
        ]);

        parent::__construct();
    }

    /**
     * TODO: persist tokens to DB or figure out how to revoke tokens using s claim
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = Auth::guard()->attempt($credentials)) {
            return Response::json(['message' => 'Invalid credentials'], 401);
        }
        /** @var User $user */
        $user = Auth::guard()->user();

        /** @var JWTGuard $jwtGuard */
        $jwtGuard = Auth::guard('api');
        return $this->respondWithToken($jwtGuard->login($user));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        /** @var User $identity */
        $identity = Auth::guard()->user();
        return Response::json([
            "identity" => $identity->getArrayCopy(),
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        Auth::guard()->logout();

        return Response::json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::guard('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return Response::json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
        ]);
    }
}
