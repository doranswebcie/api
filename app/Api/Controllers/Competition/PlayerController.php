<?php

namespace App\Api\Controllers\Competition;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\Player;
use Illuminate\Http\Request;

class PlayerController extends AbstractController
{
    const ENTITY_CLASS = Player::class;

    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
    }
}
