<?php

namespace App\Api\Controllers\Competition;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\Lol\Summoner;
use Illuminate\Http\Request;

class SummonerController extends AbstractController
{
    const ENTITY_CLASS = Summoner::class;

    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'player' => 'required',
            'tag' => 'required',
        ]);
    }
}