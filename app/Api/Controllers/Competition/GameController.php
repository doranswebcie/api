<?php

namespace App\Api\Controllers\Competition;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\Game;
use Illuminate\Http\Request;

class GameController extends AbstractController
{
    const ENTITY_CLASS = Game::class;

    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'competition' => 'required',
            'winner' => 'required',
            'loser' => 'required',
            'playedAt' => 'required',
        ]);
    }
}
