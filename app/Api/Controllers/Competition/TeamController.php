<?php

namespace App\Api\Controllers\Competition;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\ArrayCopyable;
use App\Entities\Game;
use App\Entities\Player;
use App\Entities\Team;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Illuminate\Http\Request;

class TeamController extends AbstractController
{
    const ENTITY_CLASS = Team::class;

    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'competitions' => 'required',
            'name' => 'required',
        ]);
    }

    /**
     * @inheritdoc
     */
    protected function getEntities($criteria, $orderBy)
    {
        if (isset($criteria['competitions'])) {
            /** @var EntityRepository $repository */
            $repository = $this->em->getRepository(static::ENTITY_CLASS);
            $qb = $repository->createQueryBuilder('e')
                ->where(':competition MEMBER OF e.competitions')
                ->setParameters(array('competition' => $criteria['competitions']));

            if (isset($criteria['leaderboard'])) {
                $exrp = $qb->expr();
                $qb = $qb->addSelect('COUNTIF(games.winner, e.id) AS wins')
                    ->addSelect('COUNTIF(games.loser, e.id) AS losses')
                    ->leftJoin(Game::class, 'games',
                        Join::WITH, $exrp->andX(
                            $exrp->eq('games.competition', ':competition'),
                            $exrp->orX(
                                $exrp->eq('e.id', 'games.winner'),
                                $exrp->eq('e.id', 'games.loser')
                            )
                        )
                    )
                    ->groupBy('e.id', 'e.id');
            }
            return $qb->getQuery()->getResult();
        }
        return parent::getEntities($criteria, $orderBy);
    }

    /**
     * @inheritdoc
     */
    protected function getRelatedAsArray(ArrayCopyable $related, array $includes, $property)
    {
        if (!in_array('summoner', $includes) || $property !== 'players') {
            return parent::getRelatedAsArray($related, $includes, $property);
        }

        /** @var Player $related */
        $summoner = $related->getSummoner();
        $related = $related->getArrayCopy();
        $related['summoner'] = [
            'tag' => $summoner->getTag(),
            'api' => $summoner->getApi(),
        ];

        $related['summoner']['queues'] = [];
        foreach ($summoner->getQueues() as $queue) {
            $related['summoner']['queues'][$queue->getType()] = [
                'tier' => $queue->getTier(),
                'division' => $queue->getDivision(),
                'type' => $queue->getType(),
                'updatedAt' => $queue->getUpdatedAt()->format(\DateTime::ISO8601),
            ];
        }

        return $related;
    }
}
