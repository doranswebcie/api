<?php

namespace App\Api\Controllers\Competition;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\Competition;
use Illuminate\Http\Request;

class CompetitionController extends AbstractController
{
    const ENTITY_CLASS = Competition::class;

    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//                'name2' => 'required',
        ]);
    }
}
