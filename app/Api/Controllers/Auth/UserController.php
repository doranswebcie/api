<?php

namespace App\Api\Controllers\Auth;

use App\Api\Controllers\Base\AbstractController;
use App\Entities\User;
use Doctrine\Common\Collections\Criteria;
use Illuminate\Http\Request;

class UserController extends AbstractController
{
    const ENTITY_CLASS = User::class;

    /** @var Request */
    protected $request;

    protected function validateRequest(Request $request)
    {
        $validationRules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|',
        ];

        if (strtoupper($request->getMethod()) !== 'PUT') {
            $validationRules = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:' . User::class,
                'password' => 'required|min:6|confirmed',
            ];
        }

        $this->validate($request, $validationRules);
        $this->request = $request;
    }

    protected function getOrderByParamFromRequest(Request $request)
    {
        $orderBy = parent::getOrderByParamFromRequest($request);

        $orderBy['userId'] = Criteria::DESC;
        if (isset($orderBy['id'])) {
            unset($orderBy['id']);
        }

        return $orderBy;
    }

    /**
     * @inheritdoc
     */
    public function getIdentifier($entity)
    {
        /** @var User $entity */
        return $entity->getJWTIdentifier();
    }

    protected function getEntity($id, array $include = [])
    {
        /** @var User $entity */
        $entity = parent::getEntity($id, $include);

        if ($this->request !== null &&
            strtoupper($this->request->getMethod()) === 'PUT' &&
            $entity->getEmail() !== $this->request->get('email')
        ) {
            $this->validate($this->request, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:' . User::class,
            ]);
        }

        return $entity;
    }
}
