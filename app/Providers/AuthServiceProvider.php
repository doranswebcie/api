<?php

namespace App\Providers;

use App\Auth\DoctrineUserProvider;
use App\Auth\TokenGuard;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        // Provides fix for TokenGuard having a static storage key so it works with doctrine
        \Auth::extend('token', function ($app, $name, array $config) {
            /** @var Application $app */
            // The token guard implements a basic API token based guard implementation
            // that takes an API token field from the request and matches it to the
            // user in the database or another persistence layer where users are.
            $guard = new TokenGuard(
                \Auth::createUserProvider($config['provider']),
                $app['request']
            );

            $app->refresh('request', $guard, 'setRequest');

            return $guard;
        });

        // TODO: verify if this is still required, after updating "Tymon\JWTAuth"
        // Provides fix for JWTGuard wanting a "provider->getModel()" fn to check the "prv" claim in the JWT
        \Auth::provider('doctrine', function ($app, $config) {
            /** @var Application $app */

            $entity = $config['model'];

            $em = $app['registry']->getManagerForClass($entity);

            if (!$em) {
                throw new \InvalidArgumentException("No EntityManager is set-up for {$entity}");
            }

            return new DoctrineUserProvider(
                $app['hash'],
                $em,
                $entity
            );
        });
    }
}
