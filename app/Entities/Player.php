<?php

namespace App\Entities;

use App\Entities\Lol\Summoner;
use Doctrine\ORM\Mapping as ORM;
use App\Entities\Base\AbstractEntity;

/**
 * @ORM\Entity
 */
class Player extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * TODO: this can be unidirectional and owned by Summoner, to keep this class clean
     * @ORM\OneToOne(targetEntity="App\Entities\Lol\Summoner", mappedBy="player", cascade={"persist", "remove"})
     *
     * @var Summoner
     */
    protected $summoner;

    // region generated
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Summoner
     */
    public function getSummoner()
    {
        return $this->summoner;
    }

    /**
     * @param Summoner $summoner
     */
    public function setSummoner($summoner)
    {
        $summoner->setPlayer($this);
        $this->summoner = $summoner;
    }

    // endregion
}
