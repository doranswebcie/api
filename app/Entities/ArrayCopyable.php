<?php

namespace App\Entities;

/**
 * Interface ArrayCopyable
 * @package App\Entities
 */
interface ArrayCopyable
{
    /**
     * @param bool $stringifyTimestamps
     * @return array
     */
    public function getArrayCopy($stringifyTimestamps = true);
}