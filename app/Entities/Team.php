<?php
namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping AS ORM;
use App\Entities\Base\AbstractEntity;

/**
 * @ORM\Entity
 */
class Team extends AbstractEntity
{
    /**
     * @ORM\ManyToMany(targetEntity="Competition", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="team_competition",
     *      joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="competition_id", referencedColumnName="id")}
     * )
     *
     * @var Competition[]|ArrayCollection
     */
    protected $competitions;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $coach;

    /**
     * @ORM\Column(type="string", nullable=true) // TODO: image instead of string
     */
    protected $logo;

    /**
     * @ORM\Column(type="string", nullable=true) // TODO: image instead of string
     */
    protected $picture;

    /**
     * @ORM\ManyToMany(targetEntity="Player")
     * @ORM\JoinTable(name="team_player",
     *      joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="player_id", referencedColumnName="id")}
     *      )
     *
     * @var Player[]|ArrayCollection
     */
    protected $players;

    /**
     * @ORM\ManyToMany(targetEntity="Player")
     * @ORM\JoinTable(name="team_active_player",
     *      joinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="player_id", referencedColumnName="id")}
     *      )
     *
     * @var Player[]|ArrayCollection
     */
    protected $activePlayers;

    public function __construct()
    {
        $this->competitions = new ArrayCollection();
        $this->players = new ArrayCollection();
        $this->activePlayers = new ArrayCollection();
    }

    // region generated

    /**
     * @return mixed
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * @param \App\Entities\Competition[] $entities
     */
    public function addCompetitions($entities)
    {
        /** @var Competition $entity */
        foreach ($entities as $entity) {
            $this->competitions->add($entity);
        }
    }

    /**
     * @param \App\Entities\Competition[] $entities
     */
    public function removeCompetitions($entities)
    {
        /** @var Competition $entity */
        foreach ($entities as $entity) {
            $this->competitions->removeElement($entity);
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCoach()
    {
        return $this->coach;
    }

    /**
     * @param mixed $coach
     */
    public function setCoach($coach)
    {
        $this->coach = $coach;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param \App\Entities\Player[] $entities
     */
    public function addPlayers($entities)
    {
        /** @var Player $entity */
        foreach ($entities as $entity) {
            $this->players->add($entity);
        }
    }

    /**
     * @param \App\Entities\Player[] $entities
     */
    public function removePlayers($entities)
    {
        /** @var Player $entity */
        foreach ($entities as $entity) {
            $this->players->removeElement($entity);
        }
    }

    /**
     * @return Player[]|ArrayCollection
     */
    public function getActivePlayers()
    {
        return $this->activePlayers;
    }

    /**
     * @param \App\Entities\Player[] $entities
     */
    public function addActivePlayers($entities)
    {
        /** @var Player $entity */
        foreach ($entities as $entity) {
            $this->activePlayers->add($entity);
        }
    }

    /**
     * @param \App\Entities\Player[] $entities
     */
    public function removeActivePlayers($entities)
    {
        /** @var Player $entity */
        foreach ($entities as $entity) {
            $this->activePlayers->removeElement($entity);
        }
    }

    // endregion
}
