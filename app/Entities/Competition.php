<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use App\Entities\Base\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Competition extends AbstractEntity
{
    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="competition", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var ArrayCollection
     */
    private $games;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    // region generated

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @param Game[] $entities
     */
    public function addGames($entities)
    {
        /** @var Game $entity */
        foreach ($entities as $entity) {
            $entity->setCompetition($this);
            $this->games->add($entity);
        }
    }

    /**
     * @param Game[] $entities
     */
    public function removeGames($entities)
    {
        /** @var Game $entity */
        foreach ($entities as $entity) {
            $entity->setCompetition(null);
            $this->games->removeElement($entity);
        }
    }

    // endregion
}
