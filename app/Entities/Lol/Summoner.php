<?php

namespace App\Entities\Lol;

use Doctrine\ORM\Mapping as ORM;
use App\Entities\Base\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Summoner extends AbstractEntity
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entities\Player", inversedBy="summoner", cascade={"persist", "remove"})
     */
    protected $player;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $tag;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $api;

    /**
     * @ORM\OneToMany(targetEntity="Queue", mappedBy="summoner", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var Queue[]|ArrayCollection
     */
    protected $queues;

    public function __construct()
    {
        $this->queue = new ArrayCollection();
    }

    // region generated

    /**
     * @return mixed
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param mixed $player
     */
    public function setPlayer($player)
    {
        $this->player = $player;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param mixed $api
     */
    public function setApi($api)
    {
        $this->api = $api;
    }

    /**
     * @return Queue[]|ArrayCollection
     */
    public function getQueues()
    {
        return $this->queues;
    }

    /**
     * @param \App\Entities\Lol\Queue[] $entities
     */
    public function addQueues($entities)
    {
        /** @var Queue $entity */
        foreach ($entities as $entity) {
            $entity->setSummoner($this);
            $this->queues->add($entity);
        }
    }

    /**
     * @param \App\Entities\Lol\Queue[] $entities
     */
    public function removeQueues($entities)
    {
        /** @var Queue $entity */
        foreach ($entities as $entity) {
            $entity->setSummoner(null);
            $this->queues->removeElement($entity);
        }
    }

    // endregion
}
