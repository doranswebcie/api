<?php

namespace App\Entities\Lol;

use Doctrine\ORM\Mapping as ORM;
use App\Entities\Base\AbstractEntity;

/**
 * @ORM\Entity
 */
class Queue extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Summoner", inversedBy="queues")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $summoner;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $tier;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $division;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $type;

    // region generated

    /**
     * @return mixed
     */
    public function getSummoner()
    {
        return $this->summoner;
    }

    /**
     * @param mixed $summoner
     */
    public function setSummoner($summoner)
    {
        $this->summoner = $summoner;
    }

    /**
     * @return mixed
     */
    public function getTier()
    {
        return $this->tier;
    }

    /**
     * @param mixed $tier
     */
    public function setTier($tier)
    {
        $this->tier = $tier;
    }

    /**
     * @return mixed
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param mixed $division
     */
    public function setDivision($division)
    {
        $this->division = $division;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    // endregion
}
