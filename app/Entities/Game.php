<?php
namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use App\Entities\Base\AbstractEntity;

/**
 * @ORM\Entity
 */
class Game extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Team")
     *
     */
    protected $winner;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    protected $loser;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $playedAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $extraIdentifier;

    /**
     * @ORM\ManyToOne(targetEntity="Competition", inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $competition;

    // region generated

    /**
     * @return mixed
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @param mixed $winner
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;
    }

    /**
     * @return mixed
     */
    public function getLoser()
    {
        return $this->loser;
    }

    /**
     * @param mixed $loser
     */
    public function setLoser($loser)
    {
        $this->loser = $loser;
    }

    /**
     * @return \DateTime
     */
    public function getPlayedAt()
    {
        return $this->playedAt;
    }

    /**
     * @param \DateTime $playedAt
     */
    public function setPlayedAt($playedAt)
    {
        $this->playedAt = $playedAt;
    }

    /**
     * @return mixed
     */
    public function getExtraIdentifier()
    {
        return $this->extraIdentifier;
    }

    /**
     * @param mixed $extraIdentifier
     */
    public function setExtraIdentifier($extraIdentifier)
    {
        $this->extraIdentifier = $extraIdentifier;
    }

    /**
     * @return mixed
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * @param mixed $competition
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }

    // endregion

    public function getArrayCopy($stringifyTimestamps = true)
    {
        $objectArray = parent::getArrayCopy($stringifyTimestamps = true);
        $objectArray['playedAt'] = $this->playedAt->format(\DateTime::ISO8601);
        return $objectArray;
    }
}
