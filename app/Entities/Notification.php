<?php
namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Notification extends \LaravelDoctrine\ORM\Notifications\Notification
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="user_id")
     */
    protected $user;
}