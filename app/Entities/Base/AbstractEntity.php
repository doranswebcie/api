<?php

namespace App\Entities\Base;

use App\Entities\ArrayCopyable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Timestampable\Timestampable;
use Illuminate\Contracts\Support\Jsonable;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use LaravelDoctrine\ORM\Serializers\Jsonable as JsonableTrait;

/**
 * TODO: Look into Jsonable instead of using getArrayCopy
 * @ORM\MappedSuperclass
 */
abstract class AbstractEntity implements Timestampable, Jsonable, ArrayCopyable
{

    use Timestamps, JsonableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    // region generated

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    // endregion

    /**
     * TODO: If this grows anymore, it should be moved to a static helper class
     * TODO: If statement now also bails when collection should be hydrated, so it's wrong
     * TODO: Date properties other than createdAt and updatedAt won't get stringified
     *
     * @param bool $stringifyTimestamps
     * @return array
     */
    public function getArrayCopy($stringifyTimestamps = true)
    {
        $objectArray = get_object_vars($this);

        unset($objectArray['__initializer__']);
        unset($objectArray['__cloner__']);
        unset($objectArray['__isInitialized__']);

        if (!$stringifyTimestamps) {
            return $objectArray;
        }
        $objectArray['createdAt'] = $this->createdAt->format(\DateTime::ISO8601);
        $objectArray['updatedAt'] = $this->updatedAt->format(\DateTime::ISO8601);

        foreach ($objectArray as &$value) {
            switch (true) {
                case !$value instanceof Collection && !$value instanceof AbstractEntity:
                    continue; // Bail early

                case $value instanceof Collection:
                    $value = $value->toArray();
                    foreach ($value as &$entry) {
                        if (!$entry instanceof AbstractEntity) {
                            continue;
                        }
                        $entry = $entry->getId();
                    }
                    continue; // Entity collection is turned into an array of id's

                case $value instanceof AbstractEntity:
                    $value = $value->getId();
                    continue; // Entity is turned into an id
            }
        }

        return $objectArray;
    }
}