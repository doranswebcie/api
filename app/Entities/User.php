<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use LaravelDoctrine\ORM\Auth\Authenticatable as AuthenticatableTrait;
use LaravelDoctrine\ORM\Notifications\Notifiable as NotifiableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

// TODO: Illuminate\Foundation\Auth\Access\Authorizable

/**
 * @ORM\Entity
 */
class User implements Authenticatable, CanResetPassword, JWTSubject, ArrayCopyable
{
    use AuthenticatableTrait, CanResetPasswordTrait, NotifiableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="guid", nullable=true, options={"default": "uuid_generate_v4()"})
     */
    protected $apiToken;

    /**
     * User constructor.
     * @param $values
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function __construct($values = [])
    {
        $reflectionClass = new \ReflectionClass($this);
        foreach ($values as $name => $value) {
            $setter = "set" . ucfirst($name);
            if (!$reflectionClass->hasMethod($setter)) {
                throw new \Exception(sprintf("No method %s in class (%s)", $setter, $reflectionClass->getName()));
            }
            $this->$setter($value);
        }
    }

    public function getAuthIdentifierName()
    {
        return 'userId';
    }

    public function setPassword($password)
    {
        $this->password = bcrypt($password);
    }

    // region generated

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    // endregion

    /**
     * @param mixed $apiToken
     */
    public function setApiToken($apiToken)
    {
        if (empty($apiToken) || $apiToken === null) {
            $apiToken = self::generateUuid();
        }
        $this->apiToken = $apiToken;
    }

    /**
     * TODO: add this to static helper
     * Generate v4 UUID
     *
     * Version 4 UUIDs are pseudo-random.
     */
    protected static function generateUuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * @inheritdoc
     */
    public function getArrayCopy($stringifyTimestamps = true) {
        $objectArray = get_object_vars($this);
        unset($objectArray['password']);
        unset($objectArray['rememberToken']);
        unset($objectArray['apiToken']);
        return $objectArray;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->userId;
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'name' => $this->name,
        ];
    }
}
