<?php

namespace App\Auth;

use LaravelDoctrine\ORM\Auth\DoctrineUserProvider as BaseDoctrineUserProvider;

// TODO: verify if this is still required, after updating "Tymon\JWTAuth"
class DoctrineUserProvider extends BaseDoctrineUserProvider
{
    /**
     * Gets the name of the Eloquent user model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->entity;
    }
}